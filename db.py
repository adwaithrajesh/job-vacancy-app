from __future__ import annotations

import sqlite3
from functools import lru_cache
from typing import NamedTuple


DB_INIT = '''
create table if not exists vacancies (
    vid integer primary key,
    company_name text
);

create table if not exists jobs (
    jid integer primary key,
    vid integer,
    title text,
    count integer,
    time text,
    salary integer
);

create table if not exists applicants (
    aid integer primary key,
    jid integer,
    name text,
    email text,
    age integer,
    address text
);
'''


class JobInfo(NamedTuple):
    vid: int
    jid: int
    company_name: str
    title: str
    count: int
    salary: int
    time: str


class Applicant(NamedTuple):
    age: int
    name: str
    email: str
    address: str
    job: JobInfo


class DataBase:

    def __init__(self) -> None:
        self.conn = sqlite3.connect('db.db')
        self.curr = self.conn.cursor()

        self.curr.executescript(DB_INIT)

    def __enter__(self) -> DataBase:
        return DataBase()

    def __exit__(self, exc_type, exc_value, exc_traceback) -> None:
        self.curr.close()
        self.conn.close()

    @lru_cache(maxsize=30)
    def _get_company_name(self, vid: int) -> str:
        self.curr.execute('select * from vacancies  where vid=? limit 1', (vid,))
        return self.curr.fetchall()[0][1]

    def get_job_details(self, jid: int | None = None) -> list[JobInfo]:
        if jid is None:
            self.curr.execute('select * from jobs where count > 0')
        else:
            self.curr.execute('select * from jobs where jid=?', (jid,))

        job_list = [
            JobInfo(
                jid=r[0],
                vid=r[1],
                title=r[2],
                count=r[3],
                time=r[4],
                salary=r[5],
                company_name=self._get_company_name(r[1]),
            )
            for r in self.curr.fetchall()
        ]

        return job_list

    def add_applicant(self, jid: int, name: str, email: str, age: int, address: str) -> None:
        self.curr.execute(
            'insert into applicants (jid, name, email, age, address) values (?, ?, ?, ?, ?)',
            (
                jid,
                name,
                email,
                age,
                address,
            ),
        )
        self.conn.commit()

        self.curr.execute('select * from jobs where jid=?', (jid,))
        count = self.curr.fetchall()[0][3]
        self.curr.execute('update jobs set count=? where jid=?', (count - 1, jid))
        self.conn.commit()

    def get_applicants(self) -> list[Applicant]:
        self.curr.execute('select * from applicants')
        applicants = self.curr.fetchall()

        print(self.get_job_details(2))

        return [
            Applicant(name=i[2], email=i[3], address=i[5], age=i[4], job=self.get_job_details(i[1])[0])
            for i in applicants
        ]

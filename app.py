from flask import Flask, request, render_template, redirect, url_for, session

from db import DataBase

app = Flask(__name__)
app.secret_key = 'some key'


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        with DataBase() as db:
            job_details = db.get_job_details()
        return render_template('index.html', job_details=job_details)

    if request.method == 'POST':
        session['jid'] = int(request.form.get('jid'))
        return redirect(url_for('applicants'))


@app.route('/applicants', methods=['GET', 'POST'])
def applicants():
    if request.method == 'GET':
        with DataBase() as db:
            job_details = db.get_job_details(int(session['jid']))
            print(session['jid'], job_details)
        return render_template('applicants.html', job_details=job_details[0])
    if request.method == 'POST':
        with DataBase() as db:
            db.add_applicant(
                session['jid'],
                request.form['name'],
                request.form['email'],
                int(request.form['age']),
                request.form['address'],
            )
        return redirect('/')


@app.route('/v-applicants', methods=['GET'])
def v_applications():
    with DataBase() as db:
        applicants = db.get_applicants()
    return render_template('view_applicants.html', applicants=applicants)


if __name__ == '__main__':
    app.run(debug=True)
